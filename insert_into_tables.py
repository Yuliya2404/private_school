import mysql.connector
import click

from helper import read_csv, form_insert_from_dict_tuple


@click.command()
@click.option('--host', default="localhost", help='MySQL to connect to')
@click.option('--port', default=3306, help='MySQL to connect to')
@click.option('--username', help='Database username')
@click.option('--password', help='Database password')
@click.option('--database', help='Database to user')
def insert_data(host, port, username, password, database):
    db_connector = mysql.connector.connect(user=username, password=password, host=host, port=port, database=database)
    cursor = db_connector.cursor()
    students = read_csv("students.csv")
    staff = read_csv("staff.csv")

    #Insert students
    cursor.execute(form_insert_from_dict_tuple("students", students))
    db_connector.commit()

    #Insert items
    cursor.execute(form_insert_from_dict_tuple("staff", staff))
    db_connector.commit()


if __name__ == "__main__":
    insert_data()