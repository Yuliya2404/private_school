import mysql.connector
import click


@click.command()
@click.option('--host', default="localhost", help='MySQL to connect to')
@click.option('--port', default=3306, help='MySQL to connect to')
@click.option('--username', help='Database username')
@click.option('--password', help='Database password')
@click.option('--database', help='Database to user')
def create_table(host, port, username, password, database):
    db_connector = mysql.connector.connect(user=username, password=password, host=host, port=port, database=database)
    cursor = db_connector.cursor()

    # Create staff table
    cursor.execute("""
    CREATE TABLE staff (
ID INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
NAME VARCHAR(150) NOT NULL,
POSITION VARCHAR(150) NOT NULL,
SALARY FLOAT NOT NULL
)
    """)

    # Create students table
    cursor.execute("""
    CREATE TABLE students (
ID INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
NAME VARCHAR(150) NOT NULL,
SURNAME VARCHAR(150) NOT NULL,
AVG_SCORE FLOAT NOT NULL
);
    """)

    db_connector.close()

if __name__ == "__main__":
    create_table()